import numpy as np
import argparse
import cv2
import glob
import os



CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
	"bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
	"dog", "horse", "motorbike", "person", "pottedplant", "sheep",
	"sofa", "train", "tvmonitor"]
COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))
c=0
print("[INFO] loading model.....")
net = cv2.dnn.readNetFromCaffe("MobileNetSSD_deploy.prototxt.txt", "MobileNetSSD_deploy.caffemodel")
imaglob = glob.glob(r"D:\Bhuvanesh\Project\Object Detection_Mobilenet_SSD\OBJECT DETECTION & CLASSIFICATION WITH SOUND\OBJECT DETECTION & CLASSIFICATION_ALL_IMAGES\images\factory_image\*.jpg")
for glob_img in imaglob:
        read_img=cv2.imread(glob_img)
        c=c+1
        (h, w) = read_img.shape[:2]
        blob = cv2.dnn.blobFromImage(cv2.resize(read_img, (300, 300)), 0.007843, (300, 300), 127.5)
        print ()
        print("[INFO] computing object detections...")
        net.setInput(blob)
        detections = net.forward()
        labels=''
        for i in np.arange(0, detections.shape[2]):
                confidence = detections[0, 0, i, 2]
                if confidence > 0.4:
                    idx = int(detections[0, 0, i, 1])
                    box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                    (startX, startY, endX, endY) = box.astype("int")
                    label = "{}: {:.2f}%".format(CLASSES[idx], confidence * 100)
                    print("[INFO] {}".format(label))
                    cv2.rectangle(read_img, (startX, startY), (endX, endY),COLORS[idx], 2)
                    y = startY - 15 if startY - 15 > 15 else startY + 15
                    cv2.putText(read_img, label, (startX, y),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)
                    labels=labels+str(' ')+label
                    cv2.imwrite("D:\Bhuvanesh\Project\Object Detection_Mobilenet_SSD\OBJECT DETECTION & CLASSIFICATION WITH SOUND\OBJECT DETECTION & CLASSIFICATION_ALL_IMAGES\Output\{}.jpg".format(c),read_img)
		


